#' @title Toy dataset
#' @docType data
#' @usage data(toydata)
#' @format An object of class \code{"dataframe"}.
#' \describe{
#' \item{language}{language name}
#' \item{feature_1}{toy feature 1 (categorical)}
#' \item{feature_2}{toy feature 2 (numeric)}
#' }
#' @keywords datasets
#' @examples data(toydata)
#' head(toydata)
