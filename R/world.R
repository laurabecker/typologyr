#' @title Dataset with geographic information of the world's regions
#' @docType data
#' @usage data(world)
#' @format An object of class \code{"dataframe"}.
#' \describe{
#' \item{long}{longitude (rescaled to 0-360)}
#' \item{lat}{latitude (rescaled to 0-360)}
#' \item{group}{groups to build polygones}
#' \item{order}{order to connect points and build polygones}
#' \item{region}{geographical regions}
#' \item{subregion}{geographical subregions}
#' \item{continent}{revised linguistic macroareas (Africa, Eurasia, Papunesia, Australia, North America, South America)}
#' @keywords datasets
#' @examples data(world)
#' @references   Original S code by Richard A. Becker and Allan R. Wilks. R version by Ray Brownrigg. (2018). mapdata: Extra Map Databases. R package version 2.3.0. https://CRAN.R-project.org/package=mapdata
#' @source \url{https://CRAN.R-project.org/package=mapdata}
#' @keywords datasets
#' @examples data(world)
#' head(world)
